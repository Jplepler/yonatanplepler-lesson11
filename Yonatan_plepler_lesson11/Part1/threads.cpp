#include "threads.h"


/*
Summary: 
	This function job is to use the standard library A.K.A STD to print the sentence "I_Love_threads\n"
	to the console. To implement this function we need to use the function known as 'cout' (From STD).
	Note: The printed sentence also include an new line (\n).

Input parameters:
	This function does not get any parameters whatsoever.


Output parameters: 
	this functions returns parameter of type void.


Time complexity:
	O(1)

*/
void I_Love_Threads()
{
	cout << "I_Love_threads" << endl;
}

/*
something about threads i guess. read I_Love_threads documentary for more information
*/
void call_I_Love_Threads()
{
	thread t(I_Love_Threads);
	t.join();
}


/*
This function pushes all primes in a given range into a vector
In: range of numbers, refrence to vector that will contain primes
*/
void getPrimes(int begin, int end, vector<int>& primes)
{
	bool flag = true;
	int i = 0, k = 0;
	for (i = ( begin > 2 ? begin : 2); i <= end; i++)//start form begining or 2 if begin is lower
	{
		flag = true;
		for(k = 2; k < sqrt(i) && flag; k++)
		{
			if (i % k == 0)
			{
				flag = false;
			}
		}
		if (flag)
		{
			primes.push_back(i);
		}
	}

}


/*
This function creates a thread of the getPrimes function and returns the given vector
In: range of numbers
Out: vector of primes (integers)
*/
vector<int> callGetPrimes(int begin, int end)
{
	vector<int> primes;
	thread t(getPrimes,  begin, end, ref(primes));
	auto start = chrono::high_resolution_clock::now();
	t.join();
	auto stop = chrono::high_resolution_clock::now();
	cout << "Run time is: " << (chrono::duration_cast<chrono::milliseconds>(stop - start)).count() << "ms" << endl;
	//printVector(primes);
	return primes;
}

/*
This function prins the vector
In: vector of prime numbers
*/
void printVector(vector<int> primes)
{
	unsigned int i = 0;
	for (i = 0; i < primes.size(); i++)
	{
		cout << primes[i] << endl;
	}
}


/*
This function writes all the primes from begin to end into a file
In: range of numbers, refrence to a file
*/
void writePrimesToFile(int begin, int end, ofstream& file)
{
	bool flag = true;
	int i = 0, k = 0;
	for (i = (begin > 2 ? begin : 2); i <= end; i++)//start form begining or 2 if begin is lower
	{
		flag = true;//Reset flag for next number check
		for (k = 2; k < sqrt(i) && flag; k++)
		{
			if (i % k == 0)
			{
				flag = false;//Not prime
			}
		}
		if (flag)
		{
			file << i << endl;
		}
	}
}



/*
This function calls mutiple threads to write all prime number in a certain range to  a file
In: range, number of threads, relative path to txt file
*/
void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{
	//clean file
	ofstream cleanfile(filePath);
	cleanfile.close();

	//
	ofstream file(filePath, std::fstream::in | std::fstream::out | std::fstream::app);
	vector<thread> threads;
	int segment = 0;
	int i = 0;
	unsigned int k = 0;

	//Divide into sections and put into vector
	for (i = 0; i < N; i++)
	{
		segment = begin + ((end) / N) * i;
		threads.push_back(thread(writePrimesToFile, segment, segment + (end - begin) / N, ref(file)));
	}

	for (k = 0; k < threads.size(); k++)
	{
		threads[k].join();
	}
	
	file.close();
}