#include "threads.h"



int main()
{
	//With threads
	call_I_Love_Threads();
	cout << endl << endl;
	cout << "Primes with threads" << endl;
	vector<int> primes1 = callGetPrimes(0, 1000);
	vector<int> primes2 = callGetPrimes(0, 100000);
	vector<int> primes3 = callGetPrimes(0, 1000000);
	cout << endl << endl;


	//Without threads into vector
	cout << "Primes without threads" << endl;
	vector<int> primes4;
	vector<int> primes5;
	vector<int> primes6;
	auto start = chrono::high_resolution_clock::now();
	getPrimes(0, 1000, primes4);
	auto stop = chrono::high_resolution_clock::now();
	cout << "Run time is: " << (chrono::duration_cast<chrono::milliseconds>(stop - start)).count() << "ms" << endl;
	start = chrono::high_resolution_clock::now();
	getPrimes(0, 100000, primes5);
	stop = chrono::high_resolution_clock::now();
	cout << "Run time is: " << (chrono::duration_cast<chrono::milliseconds>(stop - start)).count() << "ms" << endl;
	start = chrono::high_resolution_clock::now();
	getPrimes(0, 1000000, primes6);
	stop = chrono::high_resolution_clock::now();
	cout << "Run time is: " << (chrono::duration_cast<chrono::milliseconds>(stop - start)).count() << "ms" << endl;

	cout << endl << endl << endl << endl;

	//To files with threads
	cout << "Wrting to files functions with threads" << endl;
	start = chrono::high_resolution_clock::now();
	callWritePrimesMultipleThreads(0, 1000, "ThreadsAreWelcomeToWriteHere.txt", 10);
	stop = chrono::high_resolution_clock::now();
	cout << "Run time is: " << (chrono::duration_cast<chrono::milliseconds>(stop - start)).count() << "ms" << endl;
	
	start = chrono::high_resolution_clock::now();
	callWritePrimesMultipleThreads(0, 100000, "ThreadsAreWelcomeToWriteHere.txt", 10);
	stop = chrono::high_resolution_clock::now();
	cout << "Run time is: " << (chrono::duration_cast<chrono::milliseconds>(stop - start)).count() << "ms" << endl;

	start = chrono::high_resolution_clock::now();
	callWritePrimesMultipleThreads(0, 1000000, "ThreadsAreWelcomeToWriteHere.txt", 10);
	stop = chrono::high_resolution_clock::now();
	cout << "Run time is: " << (chrono::duration_cast<chrono::milliseconds>(stop - start)).count() << "ms" << endl;


	cout << endl << endl;

	//To file wihtout threads
	ofstream cleanfile1("NotForThreads.txt");
	cleanfile1.close();
	ofstream file1("NotForThreads.txt", std::fstream::in | std::fstream::out | std::fstream::app);
	cout << "Wrting to files functions without threads" << endl;
	start = chrono::high_resolution_clock::now();
	writePrimesToFile(0, 1000, file1);
	stop = chrono::high_resolution_clock::now();
	cout << "Run time is: " << (chrono::duration_cast<chrono::milliseconds>(stop - start)).count() << "ms" << endl;
	file1.close();

	ofstream cleanfile2("NotForThreads.txt");
	cleanfile2.close();
	ofstream file2("NotForThreads.txt", std::fstream::in | std::fstream::out | std::fstream::app);
	start = chrono::high_resolution_clock::now();
	writePrimesToFile(0, 100000, file2);
	stop = chrono::high_resolution_clock::now();
	cout << "Run time is: " << (chrono::duration_cast<chrono::milliseconds>(stop - start)).count() << "ms" << endl;
	file2.close();

	ofstream cleanfile3("NotForThreads.txt");
	cleanfile3.close();
	ofstream file3("NotForThreads.txt", std::fstream::in | std::fstream::out | std::fstream::app);
	start = chrono::high_resolution_clock::now();
	writePrimesToFile(0, 1000000, file3);
	stop = chrono::high_resolution_clock::now();
	cout << "Run time is: " << (chrono::duration_cast<chrono::milliseconds>(stop - start)).count() << "ms" << endl;
	file3.close();

	system("pause");
	return 0;
}